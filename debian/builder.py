import os
import cpickle

import sys
sys.path.append('.')
from table import conway_polynomials

db = {}
for p,n,v in conway_polynomials:
    if p not in db:
        db[p] = {}
    db[p][n] = v

dstdir = os.path.join('build')
dst = os.path.join(dstdir, 'conway_polynomials.p')
if not os.path.exists(dstdir):
    os.makedirs(dstdir)

with open(dst, 'wb') as f:
    pickle.dump(db, f, protocol=2)
